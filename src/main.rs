use std::fs::{File, FileType};
use std::io::Write;
use std::time::Duration;

use async_std::prelude::StreamExt;
use async_std::task::sleep;
use icee_rabbit_rs::{AmqpConsumer, AmqpPublisher, init, Message, QueueSettings};
use lapin::{message::Delivery, options::BasicNackOptions};
use lapin::options::BasicAckOptions;

#[async_std::main]
async fn main() {
    init();
    let queue = QueueSettings::new_durable("pipes.60e8309331fc8466ca5e09d2.60e8309331fc8466ca5e09d3-sta".into());
    let publisher = AmqpPublisher::new("".into(), "".into(), true);

    let amqp_consumer = AmqpConsumer::new(queue, None);
    loop {
        let mut consumer = amqp_consumer.connect().await;
        while let Some(delivery) = consumer.next().await {
            if let Ok((_channel, delivery)) = delivery {
                println!("requeued");
                let msg = Message::from(&delivery);
                // let target = msg.header_string("pf-repeat-queue").unwrap();
                let target = "node.60e8309331fc8466ca5e09d3.1";

                publisher.publish(msg, None, Some(target)).await;

                delivery.ack(BasicAckOptions::default()).await.expect("ack message");
            }
        }
    }
}
